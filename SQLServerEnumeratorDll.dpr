library SQLServerEnumeratorDll;

{

  Enumerating available SQL Servers. Retrieving databases on a SQL Server.

  http://delphi.about.com/library/weekly/aa090704a.htm

  Here's how to create your own connection dialog
  for a SQL Server database. Full Delphi source
  code for getting the list of available MS SQL Servers
  (on a network) and listing database names on a Server.


  ..............................................
  Zarko Gajic, BSCS
  About Guide to Delphi Programming
  http://delphi.about.com
  how to advertise: http://delphi.about.com/library/bladvertise.htm
  free newsletter: http://delphi.about.com/library/blnewsletter.htm
  forum: http://forums.about.com/ab-delphi/start/
  ..............................................

  This is a simple conversion of the above Delphi sample program to a dll

}

uses
  System.SysUtils,
  System.Classes,
  DB, ADODB, Variants, ActiveX, ComObj, AdoInt, OleDB,
  Winapi.Windows, Vcl.Dialogs,
  Math;

type
  TSQLConnection = record
    ServerName: widestring;
    DatabaseName: widestring;
    UserName: widestring;
    Password: widestring;
  end;

var
  SC: TSQLConnection;

{$R *.res}

procedure GetFPControlState(var FPExceptionMask: TArithmeticExceptionMask;
  var FPRoundMode: TRoundingMode; var FPPrecisionMode: TFPUPrecisionMode);
begin
  FPExceptionMask := GetExceptionMask;
  FPRoundMode := GetRoundMode;
  FPPrecisionMode := GetPrecisionMode;
end;

procedure SetFPControlState(FPExceptionMask: TArithmeticExceptionMask;
  FPRoundMode: TRoundingMode; FPPrecisionMode: TFPUPrecisionMode);
begin
  SetExceptionMask(FPExceptionMask);
  SetRoundMode(FPRoundMode);
  SetPrecisionMode(FPPrecisionMode);
end;

procedure ListAvailableSQLServers(Names: TStrings);
var
  RSCon: ADORecordsetConstruction;
  Rowset: IRowset;
  SourcesRowset: ISourcesRowset;
  SourcesRecordset: _Recordset;
  SourcesName, SourcesType: TField;

  function PtCreateADOObject(const ClassID: TGUID): IUnknown;
  var
    Status: HResult;
    FPExceptionMask: TArithmeticExceptionMask;
    FPRoundMode: TRoundingMode;
    FPPrecisionMode: TFPUPrecisionMode;
  begin
    GetFPControlState(FPExceptionMask, FPRoundMode, FPPrecisionMode);
    Status := CoCreateInstance(CLASS_Recordset, nil, CLSCTX_INPROC_SERVER or
      CLSCTX_LOCAL_SERVER, IUnknown, Result);
    SetFPControlState(FPExceptionMask, FPRoundMode, FPPrecisionMode);
    OleCheck(Status);
  end;

begin
  SourcesRecordset := PtCreateADOObject(CLASS_Recordset) as _Recordset;
  RSCon := SourcesRecordset as ADORecordsetConstruction;
  SourcesRowset := CreateComObject(ProgIDToClassID('SQLOLEDB Enumerator'))
    as ISourcesRowset;
  OleCheck(SourcesRowset.GetSourcesRowset(nil, IRowset, 0, nil,
    IUnknown(Rowset)));
  RSCon.Rowset := Rowset;
  with TADODataSet.Create(nil) do
    try
      Recordset := SourcesRecordset;
      SourcesName := FieldByName('SOURCES_NAME'); { do not localize }
      SourcesType := FieldByName('SOURCES_TYPE'); { do not localize }
      Names.BeginUpdate;
      try
        while not EOF do
        begin
          if (SourcesType.AsInteger = DBSOURCETYPE_DATASOURCE) and
            (SourcesName.AsString <> '') then
            Names.Add(SourcesName.AsString);
          Next;
        end;
      finally
        Names.EndUpdate;
      end;
    finally
      Free;
    end;
end;

function GetConnStr(var AServer: widestring; AUserName, APassword: widestring)
  : widestring;
begin
  SC.ServerName := AServer;
  SC.DatabaseName := '';
  SC.UserName := AUserName;
  SC.Password := APassword;

  Result := 'Provider=SQLOLEDB.1;Persist Security Info=False;';
  Result := Result + 'Data Source=' + SC.ServerName + ';';
  if SC.DatabaseName <> '' then
    Result := Result + 'Initial Catalog=' + SC.DatabaseName + ';';

  if AUserName <> '' then
  begin
    Result := Result + 'uid=' + SC.UserName + ';';
    Result := Result + 'pwd=' + SC.Password + ';';
  end
  else
  begin
    Result := Result + 'Integrated Security=SSPI;';
  end

end;

procedure DatabasesOnServer(var Databases: TStringList;
  AServer, AUserName, APassword: widestring);
var
  rs: _Recordset;
begin
  Databases.Clear;
  with TAdoConnection.Create(nil) do
    try
      ConnectionString := GetConnStr(AServer, AUserName, APassword);
      LoginPrompt := False;
      try
        Open;
        rs := ConnectionObject.OpenSchema(adSchemaCatalogs, EmptyParam,
          EmptyParam);
        with rs do
        begin
          try
            Databases.BeginUpdate;
            while not EOF do
            begin
              Databases.Add(VarToStr(Fields['CATALOG_NAME'].Value));
              MoveNext;
            end;
          finally
            Databases.EndUpdate;
          end;
        end;
        Close;
      except
        on e: exception do
          MessageDlg(e.Message, mtError, [mbOK], 0);
      end;
    finally
      Free;
    end;
end;

function GetAvailableSQLServers(out Names: widestring): BOOL; stdcall;
var
  MyStringList: TStringList;
begin
  MyStringList := nil;
  MyStringList := TStringList.Create;
  MyStringList.Delimiter := '|';
  ListAvailableSQLServers(MyStringList);
  Names := MyStringList.DelimitedText;
  MyStringList.Free;
  Result := True;
end;

function GetDatabasesOnServer(out Databases: widestring;
  AServer, AUserName, APassword: widestring): BOOL; stdcall;
var
  MyStringList: TStringList;
begin
  MyStringList := nil;
  MyStringList := TStringList.Create;
  MyStringList.Delimiter := '|';
  DatabasesOnServer(MyStringList, AServer, AUserName, APassword);
  Databases := MyStringList.DelimitedText;
  MyStringList.Free;
  Result := True;
end;

exports
  GetAvailableSQLServers,
  GetDatabasesOnServer;

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;

end.
