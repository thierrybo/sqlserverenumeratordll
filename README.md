# SqlServerEnumeratorDll

Delphi DLL conversion from SqlServerEnumerator Delphi application https://web.archive.org/web/20150907013136/http://delphi.about.com/od/sqlservermsdeaccess/l/aa090704a.htm

I needed this because I did not succeeded to do it  with [Lazarus!](https://www.lazarus-ide.org/).

## Description
The DLL provides two functions:

**GetAvailableSQLServers** obtain a list of SQL Servers on a network and returns a BSTR delimited with ```|``` character.

**GetDatabasesOnServer** obtain a list of databases for one named SQL Server instance and returns a BSTR delimited with ```|``` character.


I'm a Delphi/Lazarus user, so parameters are [BSTR](https://docs.microsoft.com/fr-fr/previous-versions/windows/desktop/automat/bstr?redirectedfrom=MSDN) because it is much easier to handle from Pascal than PChar.

## Installation
Copy th DLL in the same folder as you exe.

## Usage
```function GetAvailableSQLServers(out Names : WideString): BOOL; stdcall;```
**Names**: BSTR that will be populated with SQL Server instances names, separated with ```|```` character.

```function GetDatabasesOnServer(out Databases: WideString; AServer, AUserName, APassword: WideString): BOOL; stdcall;```
**Databases**: BSTR that will be populated with databases names, separated with ```|```` character.

**AServer**: BSTR with SQL Server instance name to look for databases.

**AUserName, APassword**: BSTR credentials for SQL Server authentication, empty if using SSPI authentication.

Sample Lazarus code:

```
function GetAvailableSQLServers(out Names: WideString): BOOL; stdcall;
  external 'SQLServerEnumeratorDll.dll';
function GetDatabasesOnServer(out Databases: WideString;
  AServer, AUserName, APassword: WideString): BOOL;
  stdcall; external 'SQLServerEnumeratorDll.dll';

implementation

.....


procedure TMyForm.MyComboClick(Sender: TObject);
var
  sAvailableServers: WideString;
begin
  Screen.Cursor := crSQLWait;
  try
    GetAvailableSQLServers(sAvailableServers);
    MyCombo.Items.Delimiter := '|';
    MyCombo.Items.DelimitedText := UTF8Encode(sAvailableServers);
  finally
    Screen.Cursor := crDefault;
  end;
  MyCombo.DroppedDown := True;
end;

procedure TMyForm.MyCombo2Select(Sender: TObject);
var
  sDatabases, sSelectedServer: WideString;
  slDatabases: TStringList;
  sUserName, sPassword: string;
begin
  Screen.Cursor := crSQLWait;
  sDatabases := '';
  sSelectedServer := UTF8Decode(MyCombo.Text);

  if rbUseSspi.Checked then
  begin
    sUserName := '';
    sPassword := '';
  end
  else
  begin
    sUserName := lbeSqlUser.Text;
    sPassword := lbeSqlPwd.Text;
  end;
  GetDatabasesOnServer(sDatabases, sSelectedServer, UTF8Decode(sUserName),
    UTF8Decode(sPassword));

  slDatabases := nil;
  slDatabases := TStringList.Create;
  try
    slDatabases.Delimiter := '|';
    slDatabases.DelimitedText := UTF8Encode(sDatabases);
    MyCombo2.Items := slDatabases;
  finally
    slDatabases.Free;
  end;
  Screen.Cursor := crDefault;
end;

```